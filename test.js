const expect    = require("chai").expect;
const Immutable = require('immutable');

describe('Manage state with immutable.js', () => {

  it ('should see side effect when mutating original array',() => {
    const original = ['item1', 'item2'];
    const second = original;
    second[0] = 'item3';
    expect(original[0]).to.equal('item3');
  });

  it('should prevent mutation of original array', () => {
    const original = Immutable.List(['item1', 'item2']);
    const second = original;
    second.set(0, 'item3');
    expect(original.get(0)).to.equal('item1');
  });

  it('should return a new immutable Map everytime a map is set', () => {
    let data = {'foo': 'bar'};
    let data2 = {'foo1': 'bar1'};

    let map1 = Immutable.Map();
    let map2 = map1.set('1', data);
    let map3 = map2.set('2', data2);


    expect(map1.get('1')).to.be.undefined;

    expect(map2.get('1').foo).to.equal('bar');
    expect(map2.get('2')).to.be.undefined;

    expect(map3.get('1').foo).to.equal('bar');
    expect(map3.get('2').foo1).to.equal('bar1');

  });

  it('should return a new immutable Map everytime a delete operation is run', () => {

    let data = {'foo': 'bar'};

    const map1 = Immutable.Map({
      '1': data,
      '2': data
    });

    const map2 = map1.delete('2');

// map1 will remain unchanged
    expect(map1.get('2').foo).to.equal('bar');

//map2 will contain a new map but without the deleted key
    expect(map2.get('2')).to.be.undefined;



  });

  it('should generate a new Immutable Map when attempting to update the original map', () => {
    const originalMap = Immutable.Map({
      key: 'value'
    });

    const newMap = originalMap.update('key', value => {
      return value + value;
    });

    expect(newMap.get('key')).to.equal('valuevalue');

  });

  it('should be able to merge two immutable structures into a third one. ', () => {

    var x = Immutable.Map({a: 10, b: 20, c: 30});
    var y = Immutable.Map({b: 40, a: 50, d: 60});

    // if there are similar properties in x and y then y properties will overwrite those of x
    var z = x.merge(y) // { a: 50, b: 40, c: 30, d: 60 }

    expect(z.get('b')).to.equal(40);

  });

  it('clear function will create a new Immutable map of size 0. The original map will remain unchanged', () => {

    var x = Immutable.Map({a: 10, b: 20, c: 30});
    expect(x.size).to.equal(3);
    var y = x.clear();
    expect(x.size).to.equal(3);
    expect(y.size).to.equal(0);


  });

  // has looks up something based on key
  it('should have a "has" method that checks whether an item exists in the Map. This does not return the item', ()=>{

    var x = Immutable.Map({a: 10, b: 20, c: 30});
    expect(x.has('a')).to.equal(true);
  })

  //includes looks up something based on value
  it('should lookup something based on value', ()=> {

    var aObj = {a: 10, b: 20, c: 30};
    var bObj = {foo: 'bar'};
    var x = Immutable.Map(aObj);
    var y = x.merge(bObj);
    expect(y.has('foo')).to.equal(true);
    expect(y.includes('bar')).to.equal(true);

  });

  it('should return the value of the last item in data structure', () => {
    var x = Immutable.Map({a: 10, b: 20, c: 30});
    expect(x.last()).to.equal(30);
  });

  it('should return the value of the fist item in data structure', ()=> {
    var x = Immutable.Map({a: 10, b: 20, c: 30});
    expect(x.first()).to.equal(10);

  });


});